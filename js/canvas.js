const canvas = document.getElementById('c1');
const typePen = document.getElementById('type-pen');
const ctx = canvas.getContext('2d');
const imgItem = document.querySelector('.img-list');
const link = document.querySelector('#link');
const linkList = document.querySelector('#list-link');
const imgFolder = document.querySelector('.img-folder');
let myColor = 'black';
let size = 10;
let imageName = 'My-image';

document.querySelector('body').onload = function () {
    myColor = document.querySelector('#color').value;
    document.querySelector('.size-title').innerHTML = document.querySelector('#size').value;
    myColor = document.querySelector('.size-title').value;
    size = document.querySelector('#size').value;
    if (!document.querySelectorAll(".list-button").length) {
        imgItem.setAttribute('class', 'not-active');
        imgFolder.setAttribute('class', 'not-active');
    }
};

let PaintFunction = {
    x: null,
    y: null,
    beginPaint(argument) {
        canvas.onmousedown = function (event) {
            canvas.onmousemove = function (event) {
                if (argument === 'rect') {
                    this.x = event.offsetX;
                    this.y = event.offsetY;
                    ctx.beginPath();
                    ctx.fillRect(this.x - 5, this.y - 5, size, size);
                    ctx.fillStyle = myColor;
                    ctx.fill();
                }
                else if (argument === 'circle') {
                    this.x = event.offsetX;
                    this.y = event.offsetY;
                    ctx.beginPath();
                    ctx.arc(this.x, this.y, size, 0, 2 * Math.PI, false);
                    ctx.fillStyle = myColor;
                    ctx.fill();
                }

            };
            canvas.onmouseup = function () {
                canvas.onmousemove = null;
            }
        };
    }
}

PaintFunction.beginPaint('rect');



document.querySelector('#color').oninput = function () {
    myColor = this.value;
};

document.querySelector('#image-name').oninput = function () {
    imageName = this.value;
};

document.querySelector('#size').oninput = function () {
    size = this.value;
    document.querySelector('.size-title').innerHTML = size;
};



typePen.onchange = function () {
    switch (this.value) {
        case 'rect':
            PaintFunction.beginPaint('rect');
            break;
        case 'circle':
            PaintFunction.beginPaint('circle');
            break;

    }
};


link.onclick = function () {
    link.setAttribute('download', imageName + ".png");
    link.setAttribute('href', canvas.toDataURL("image/png").replace("image/png", "image/octet-stream"));
};

linkList.onclick = function () {
    let buttonItem = document.createElement('button');
    buttonItem.setAttribute('data-img', canvas.toDataURL("image/png").replace("image/png", "image/octet-stream"));
    buttonItem.setAttribute('class', 'list-button');
    buttonItem.innerHTML = imageName;
    imgItem.appendChild(buttonItem);
    imgItem.setAttribute('class', 'img-list clearfix');
    imgFolder.setAttribute('class', 'img-folder');
    let buttonList = document.querySelectorAll(".list-button");
    for (let i = 0; i < buttonList.length; i++) {
        buttonList[i].onclick = function (ev) {
            for (let p = 0; p < buttonList.length; p++) {
                buttonList[p].setAttribute('class', 'list-button');
            }
            ev.target.setAttribute('class', 'list-button list-button__active-color')
            imgFolder.style.backgroundImage = "url('" + ev.target.getAttribute('data-img') + "')";
        };
    }

};





